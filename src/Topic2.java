public class Topic2 {

  public static void main(String[] args) {
    System.out.println("===== Program Start with " + args[0] + " =====");
    System.out.println(":: method 1 ::");
    method1();

    System.out.println(":: method 2 ::");
    String pertama = "Pertama";
    method2(pertama);
    method2("Kedua");
    method2("Ketiga");

    System.out.println(":: method 3 ::");
    String coba = method3("Cobain");
    System.out.println(coba);
    System.out.println(method3("Langsung gaskeun"));

    // tolong implementasi penghapusan karakternya dengan lebih baik
    System.out.println(":: method 4 ::");
    String cobaLagi = replaceChar("Coba Kata Baru", "a");
    System.out.println(cobaLagi);
    System.out.println(replaceChar("Tes test aje", "e"));

    String abc = "abc";
    String _abc = "abc underscore";

    String abcaku = "abc";

    // TODO: tambahin code nya woy
  }

  public static void method1() {
    System.out.println("ini method1");
  }

  public static void method2(String par) {
    System.out.println(par);
  }

  private static String method3(String par) {
    return par;
  }

  /**
   * Method yang digunakan untuk menghapus salah satu karakter dari suatu text
   * @param par Text yang ingin dihapus salah satu karakternya
   * @param charHapus Karakter yang dihapus
   * @return Text yang sudah terhapus salah satu karakter
   */
  private static String replaceChar(String par, String charHapus) {
    String hasil = par.replaceAll(charHapus, "");
    return hasil;
  }

  private static void testTambahMethod() {
    String varTest = "tambah method aja";
    System.out.println(varTest);
  }

  public static int fiturA() {
    // do something for fitur a
    return 0;
  }

  public static String fiturB() {
    return "fitur b";
  }

}
