import java.util.Scanner;

public class Topic3 {

  String globalVar = "ini global variable";
  public static String classVariable = "class Variable";
  public String instanceVariable = "instance variable";

  public static void main(String[] args) {
    System.out.println("===== Tipe Data =====");
    tipeData();

    System.out.println("===== Kalkulator =====");
    kalkulator();

    System.out.println("===== Array =====");
    array();

    System.out.println("===== Operator =====");
    operator();

    System.out.println("===== Conditional =====");
    ifElse();
    switchCase();

    System.out.println("===== Loop =====");
    forLoop();
    whileLoop();
  }

  public static void array() {
    int array1[] = new int[5];
    int[] array2 = new int[5];
    // {int, int, int, int, int}
    // {0, 0, 0, 0, 0}
    int[] array3 = {1, 2, 3, 4, 5};
    System.out.println("test index : " + array3[3]);

    array3[3] = 99;
    System.out.println("test index : " + array3[3]);

    // panggil array3 dari index 0 sampai index 4
    System.out.println("index : " + array3[0]);
    System.out.println("index : " + array3[1]);
    System.out.println("index : " + array3[2]);
    System.out.println("index : " + array3[3]);
    System.out.println("index : " + array3[4]);

    System.out.println(array3[0] + array3[4]);

  }

  public static void operator() {
    int a = 9;
    int b = 4;
    System.out.println("hasil bagi tidak bulat dari int : " + (a / b));
    System.out.println("hasil modulus : " + (a%b));
    System.out.println(Math.pow(2, 4));

    System.out.println(++b); // +1 +b = 5
    System.out.println(a++); // a +1 = 9
    System.out.println(a); // 10

    System.out.println("===== Compund Assignment =====");
    int opr1 = 9;
    opr1 += 5; // 9 + 5 ---- opr1 = 14
    System.out.println(opr1);
    opr1 -= 2; // 14 - 2 ---- opr1 = 12
    System.out.println(opr1);
    opr1 *= 2; // 12 * 2 ---- opr1 = 24
    System.out.println(opr1);
    opr1 /= 3; // 24 / 3 ---- opr1 = 8
    System.out.println(opr1);
    opr1 %= 7; // 8 % 7 ---- opr1 = 1
    System.out.println(opr1);

    System.out.println("===== Comparison =====");
    int comp1 = 5;
    int comp2 = 10;
    System.out.println(comp1 == comp2); // false --- karena 5 tidak sama dengan 10
    System.out.println(comp1 != comp2); // true --- karena 5 memang tidak sama dengan 10
    System.out.println(comp1 < comp2); // true --- karena 5 kurang dari 10
    System.out.println(comp1 <= comp2); // true --- karena 5 kurang dari sama dengan 10

    System.out.println("===== Logical =====");
    int log1 = 1;
    int log2 = 2;
    System.out.println(comp1 == comp2 && log1 != log2); // false
  }

  public static void kalkulator() {
    System.out.println("=============================");
    System.out.println("Kalkulator yg cuma bisa nambah");
    Scanner input = new Scanner(System.in);
    System.out.print("Masukkan nilai pertama : ");
    int nilaiPertama = input.nextInt();
    System.out.print("Masukkan nilai kedua : ");
    int nilaiKedua = input.nextInt();
    System.out.println("Hasil penambahan dari " + nilaiPertama + " dan " + nilaiKedua + " adalah : " + addition(nilaiPertama, nilaiKedua));
    input.close();
  }

  /**
   * Method yang menjelaskan handson mengenai tipe data & variable
   */
  public static void tipeData() {
    System.out.println("===== tipe data char =====");
    char tandaTanyaInAscii = '\77';
    char tandaTanya = '?';

    System.out.println(tandaTanyaInAscii);
    System.out.println(tandaTanya);

    System.out.println("===== tipe data round number =====");
    byte numByte = 10;
    short numShort = 1000;
    int numInt = 10000;
    long numLong = 100000000l;

    System.out.println("===== tipe data fraction number =====");
    float numFloat = 2.5f;
    double numDouble = 2.66666d;

    System.out.println("===== tipe data boolean =====");
    boolean benar = true;
    boolean logic = 10 > 5; // dia akan kasih nilai true karena 10 lebih dari 5
  }

  public void localVar() {
    String localVariable = "ini adalah local variable";
    System.out.println(localVariable);
    System.out.println(globalVar);
  }

  public void caller() {
    System.out.println(globalVar);
  }

  public static int addition(int a, int b) {
    return a + b;
  }

  public static void ifElse() {
    int valA = 5;
    int valB = 6;

    // Conditional if else biasa
    String hasil = "";
    if(valA < valB) {
      hasil = "valA kurang dari valB";
    } else {
      hasil = "valA tidak kurang dari valB nih";
    }
    System.out.println(hasil);

    //Conditional if else ternary
    String hasilTernary = valA < valB ? "valA kurang dari valB nih" : "valA ternyata ga kurang nih dari valB";
    System.out.println(hasilTernary);

    // Konversi nilai angka ke huruf dengan else if
    int nilai = 85;
    String nilaiHuruf = "";
    if(nilai >= 80) {
      nilaiHuruf = "A";
    } else if(nilai >= 70 && nilai < 80) {
      nilaiHuruf = "B";
    } else if(nilai >= 60 && nilai < 70) {
      nilaiHuruf = "C";
    } else if(nilai >= 50 && nilai < 60) {
      nilaiHuruf = "D";
    } else {
      nilaiHuruf = "E";
    }
    System.out.println(nilaiHuruf);

    // Don't do this!!!
    String nilaiHurufTernary = nilai >= 80 ? "A" : nilai >= 70 && nilai < 80 ? "B" : "E";

    valA = 10;
    // Conditional if tanpa else jadi bisa 1 baris
    if (valA > valB) System.out.println("val A lebih dari valB");
  }

  public static void switchCase() {
    String nilai = "D";
    switch(nilai) {
      case "A" :
        System.out.println("Selamat anda dapat nilai A");
        break;
      case "B" :
        System.out.println("Anda dapat nilai B");
        break;
      default :
        System.out.println("ERROR ini apaan");
        break;
    }
  }

  public static void forLoop() {
    int[] data = {1, 2, 3, 4, 5};
    int[] sumData = new int[5];
    for(int index = 0; index < data.length; index++) {
      int sum = data[index];
      for(int index2 = index+1; index2 < data.length; index2++) {
        sum += data[index2];
      }
      sumData[index] = sum;
    }

    for(int i = 0; i < sumData.length; i++) {
      System.out.print(sumData[i] + " ");
    }
  }

  public static void whileLoop() {
    int singleData = 100;
    while(singleData % 2 == 0) {
      // Do something
      System.out.println("single data : " + singleData);
      singleData--;
    }

    do {
      // do something
    } while(singleData % 2 == 0);
  }

  public static int recursion(int k) {
    if(k > 0) {
      // k + (k-1) + (k-2) + (k-3) ... + (k-n)
      return k + recursion(k - 1);
    } else {
      return 0;
    }
  }

}
